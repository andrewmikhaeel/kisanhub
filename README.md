# README #

### What is this repository for? ###

This repository was created as an interview challenge for KisanHub

### Problem definition ###

Here at Kisanhub we use Django and Django Rest Framework (DRF) to implement all the APIs used by our Angular client and mobile apps. The input data for our system originates from multiple sources - either directly from user input or via external APIs.

Your mission is to create a Django app using DRF to store and retrieve UK weather data.


### How to start ###
* Use your favorite package manager to install the requirements
    ```
    pip3 install -r requirements.txt
    or
    pipenv install
    ```
* Migrate database
    ```
    ./manage.py migrate
    ```
* Create super user
    ```
    ./manage.py createsuperuser
    ```
    follow the prompts to choose an email and password. This will be used to access the admin site
* Run django server locally
    ```bash
    ./manage.py runserver
    ```
    Note: you might need to create super user

* Run the tests
    ```commandline
    ./manage.py test
    ```
* Populate your database
    ```commandline
    ./manage.py populatedb
    ```

### API Options
# Metrics
```json
URL: /weather/v1/metrics/

Allow: GET, POST, HEAD, OPTIONS
Content-Type: application/json
Vary: Accept

{
    "name": "Metrics",
    "description": "",
    "renders": [
        "application/json",
        "text/html"
    ],
    "parses": [
        "application/json",
        "application/x-www-form-urlencoded",
        "multipart/form-data"
    ],
    "actions": {
        "POST": {
            "value": {
                "type": "float",
                "required": true,
                "read_only": false,
                "label": "Value"
            },
            "month": {
                "type": "integer",
                "required": true,
                "read_only": false,
                "label": "Month",
                "min_value": 1,
                "max_value": 12
            },
            "year": {
                "type": "integer",
                "required": true,
                "read_only": false,
                "label": "Year",
                "min_value": 1800,
                "max_value": 9999
            },
            "metric_type": {
                "type": "string",
                "required": true,
                "read_only": false,
                "label": "Metric type",
                "max_length": 23
            },
            "region": {
                "type": "string",
                "required": true,
                "read_only": false,
                "label": "Region",
                "max_length": 23
            }
        }
    }
}
```
    
```json
URL: /weather/v1/metric_types/
Allow: GET, POST, HEAD, OPTIONS
Content-Type: application/json
Vary: Accept

{
    "name": "Metric Types",
    "description": "",
    "renders": [
        "application/json",
        "text/html"
    ],
    "parses": [
        "application/json",
        "application/x-www-form-urlencoded",
        "multipart/form-data"
    ],
    "actions": {
        "POST": {
            "name": {
                "type": "string",
                "required": true,
                "read_only": false,
                "label": "Name",
                "max_length": 63
            },
            "unit": {
                "type": "string",
                "required": true,
                "read_only": false,
                "label": "Unit",
                "max_length": 15
            }
        }
    }
}
```
```json
URL: /weather/v1/regions/

HTTP 200 OK
Allow: GET, POST, HEAD, OPTIONS
Content-Type: application/json
Vary: Accept

{
    "name": "Regions",
    "description": "",
    "renders": [
        "application/json",
        "text/html"
    ],
    "parses": [
        "application/json",
        "application/x-www-form-urlencoded",
        "multipart/form-data"
    ],
    "actions": {
        "POST": {
            "name": {
                "type": "string",
                "required": true,
                "read_only": false,
                "label": "Name",
                "max_length": 63
            }
        }
    }
}

```