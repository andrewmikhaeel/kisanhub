from django.test import TestCase
from rest_framework.test import APIClient
from django.urls import reverse
from rest_framework import status
from datetime import date
from random import randrange
from .constants import *
from .models import Region, MetricType, Metric


class MetricsViewTestCase(TestCase):
    REGIONS = ["test region 1", "test region 2"]
    TYPES = ["test type 1", "test type 2"]
    DATES = [date(2020, 3, 16), date(1993, 9, 12), date(2001, 1, 1), date(2030, 1, 1)]

    def setUp(self):
        self.client = APIClient()
        for i in range(2):
            MetricType(name=self.TYPES[i], unit='mm').save()
            Region(name=self.REGIONS[i]).save()

        for i in range(4):
            Metric(value=randrange(100), date=self.DATES[i], region_id=self.REGIONS[i % 2],
                   metric_type_id=self.TYPES[int(i / 2)]).save()

    def test_create_metric(self):
        """
        test creating a new metric successfully
        """
        old_metrics_count = Metric.objects.count()
        data = {
            KEY_VALUE: 5,
            KEY_YEAR: 1980,
            KEY_MONTH: 2,
            KEY_TYPE: self.TYPES[0],
            KEY_REGION: self.REGIONS[0]
        }
        response = self.client.post(reverse('metrics'), data=data)
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)
        self.assertEquals(Metric.objects.count(), old_metrics_count + 1)

    def test_create_metric_invalid_month(self):
        """
        test creating a metric with invalid month, should return error response with a message.
        also shouldn't create a new object
        """
        old_metrics_count = Metric.objects.count()
        data = {
            KEY_VALUE: 5,
            KEY_YEAR: 1950,
            KEY_MONTH: 45,
            KEY_TYPE: self.TYPES[0],
            KEY_REGION: self.REGIONS[0]
        }
        response = self.client.post(reverse('metrics'), data=data)
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue(KEY_MONTH in response.json())
        self.assertEquals(Metric.objects.count(), old_metrics_count)

    def test_create_metric_invalid_year(self):
        """
        test creating a metric with invalid year, should return error response with a message.
        also shouldn't create a new object
        """
        old_metrics_count = Metric.objects.count()
        data = {
            KEY_VALUE: 5,
            KEY_YEAR: 0,
            KEY_MONTH: 11,
            KEY_TYPE: self.TYPES[0],
            KEY_REGION: self.REGIONS[0]
        }
        response = self.client.post(reverse('metrics'), data=data)
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue(KEY_YEAR in response.json())
        self.assertEquals(Metric.objects.count(), old_metrics_count)

    def test_create_metric_invalid_type(self):
        """
        test creating a metric with invalid type, should return error response with a message.
        also shouldn't create a new object
        """
        old_metrics_count = Metric.objects.count()
        data = {
            KEY_VALUE: 5,
            KEY_YEAR: 1950,
            KEY_MONTH: 11,
            KEY_TYPE: "invalid",
            KEY_REGION: self.REGIONS[0]
        }
        response = self.client.post(reverse('metrics'), data=data)
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue(KEY_TYPE in response.json())
        self.assertEquals(Metric.objects.count(), old_metrics_count)

    def test_create_metric_invalid_region(self):
        """
        test creating a metric with invalid region, should return error response with a message.
        also shouldn't create a new object
        """
        old_metrics_count = Metric.objects.count()
        data = {
            KEY_VALUE: 5,
            KEY_YEAR: 1950,
            KEY_MONTH: 11,
            KEY_TYPE: self.TYPES[0],
            KEY_REGION: "invalid"
        }
        response = self.client.post(reverse('metrics'), data=data)
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue(KEY_REGION in response.json())
        self.assertEquals(Metric.objects.count(), old_metrics_count)

    def test_get_all_metrics(self):
        """
        get without query_params should return all the metrics
        """
        response = self.client.get(reverse('metrics'))
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(len(response.json()), Metric.objects.count())

    def test_get_metrics_filtered_by_end_date_only(self):
        """
        get with end_date should return all metrics before that date
        """
        end_date = date(2020, 5, 5)
        response = self.client.get(reverse('metrics'), data={KEY_END_DATE: str(end_date)})
        self.assertGreater(len(response.json()), 0)
        for entry in response.json():
            entry_date = date(entry[KEY_YEAR], entry[KEY_MONTH], DEFAULT_DAY)
            self.assertGreaterEqual(end_date, entry_date)

    def test_get_metrics_filtered_by_start_date_only(self):
        """
        get with start_date should return all metrics after that date
        """
        start_date = date(2000, 5, 5)
        response = self.client.get(reverse('metrics'), data={KEY_START_DATE: str(start_date)})
        self.assertGreater(len(response.json()), 0)
        for entry in response.json():
            entry_date = date(entry[KEY_YEAR], entry[KEY_MONTH], DEFAULT_DAY)
            self.assertLessEqual(start_date, entry_date)

    def test_get_metrics_filtered_by_date_range(self):
        """
        get with end_date and start_date should return all metrics in this date range
        """
        end_date = date(2020, 5, 5)
        start_date = date(2000, 5, 5)
        response = self.client.get(reverse('metrics'),
                                   data={KEY_START_DATE: str(start_date), KEY_END_DATE: str(end_date)})
        for entry in response.json():
            entry_date = date(entry[KEY_YEAR], entry[KEY_MONTH], DEFAULT_DAY)
            self.assertLessEqual(start_date, entry_date)
            self.assertGreaterEqual(end_date, entry_date)

    def test_get_metrics_filtered_by_region(self):
        """
        get with region should return all metrics after that region
        """
        region = self.REGIONS[0]
        response = self.client.get(reverse('metrics'), data={KEY_REGION: region})
        self.assertGreater(len(response.json()), 0)
        for entry in response.json():
            self.assertEquals(region, entry[KEY_REGION])

    def test_get_metrics_filtered_by_type(self):
        """
        get with type should return all metrics after that type
        """
        type = self.TYPES[0]
        response = self.client.get(reverse('metrics'), data={KEY_TYPE: type})
        self.assertGreater(len(response.json()), 0)
        for entry in response.json():
            self.assertTrue(type in entry[KEY_TYPE])

