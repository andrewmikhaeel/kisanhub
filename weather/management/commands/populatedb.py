from django.core.management.base import BaseCommand, CommandError
from django.urls import reverse
from weather.constants import *
from rest_framework import status
import requests


class Command(BaseCommand):
    SOURCE_URL = "https://storage.googleapis.com/kisanhub-interview-question-data/metoffice/"
    REGIONS = ["England", "Scotland", "Wales"]
    TYPES = [("Tmax", "C"), ("Tmin", "C"), ("Rainfall", "mm")]
    HOST = "http://localhost:8000"

    def add_arguments(self, parser):
        parser.add_argument('-H', '--host', type=str, help='API Host domain')
        parser.add_argument('-S', '--source', type=str, help='Source folder domain')

    def handle(self, *args, **options):
        self.HOST = options.get("host") or self.HOST
        self.SOURCE_URL = options.get("source") or self.SOURCE_URL
        self.populate_regions_and_types()
        metrics_api_url = reverse(URL_METRICS)
        records_count = 0
        failed_count = 0
        for region in self.REGIONS:
            for type_name, _ in self.TYPES:
                url = f"{self.SOURCE_URL}{type_name}-{region}.json"
                response = requests.get(url)
                if response.status_code != status.HTTP_200_OK:
                    print(f"Couldn't get source content for region: {region} and metric_type:{type_name}, "
                          f"using url: {url} - SKIPPING")
                    continue
                content = response.json()
                for item in content:
                    records_count += 1
                    item[KEY_TYPE] = type_name
                    item[KEY_REGION] = region
                    api_response = requests.post(self.HOST + metrics_api_url, data=item)
                    if api_response.status_code != status.HTTP_201_CREATED:
                        failed_count += 1

        print(f"""
        ============================================
            Population script exited successfully
          failed records: {failed_count}
          succeeded reccords :{records_count - failed_count}
          total : {records_count}
        ============================================
        """)

    def populate_regions_and_types(self):
        types_url = reverse(URL_METRIC_TYPES)
        region_url = reverse(URL_REGIONS)
        for type_name, unit in self.TYPES:
            response = requests.post(self.HOST + types_url, data={KEY_NAME: type_name, KEY_UNIT: unit})
            if response.status_code != status.HTTP_201_CREATED:
                content = response.json()
                if KEY_NAME in content and content[KEY_NAME] == ["metric type with this name already exists."]:
                    print(f"metric type with this name: {type_name} already exists. - SKIPPING")
                else:
                    print(f"an error happened while creating metric type: {type_name} \n {content}")
        for region in self.REGIONS:
            response = requests.post(self.HOST + region_url, data={KEY_NAME: region})
            if response.status_code != status.HTTP_201_CREATED:
                content = response.json()
                if KEY_NAME in content and content[KEY_NAME] == ["region with this name already exists."]:
                    print(f"region with this name: {type_name} already exists. - SKIPPING")
                else:
                    print(f"an error happened while creating region: {type_name} \n {content}")
