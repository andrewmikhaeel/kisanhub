KEY_VALUE = "value"
KEY_YEAR = "year"
KEY_MONTH = "month"
KEY_TYPE = "metric_type"
KEY_REGION = "region"
KEY_NAME = "name"
KEY_UNIT = "unit"
KEY_START_DATE = "start_date"
KEY_END_DATE = "end_date"

DEFAULT_DAY = 1

MAX_YEAR = 9999
MAX_MONTH = 12

MIN_YEAR = 1800
MIN_MONTH = 1

URL_METRICS = "metrics"
URL_METRIC_TYPES = "metric_types"
URL_REGIONS = "regions"
