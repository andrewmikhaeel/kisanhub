from django.urls import path
from .views import MetricsView, MetricTypesView, RegionsView
from .constants import URL_METRIC_TYPES, URL_METRICS, URL_REGIONS

urlpatterns = [
    path('metrics/', MetricsView.as_view(), name=URL_METRICS),
    path('metric_types/', MetricTypesView.as_view(), name=URL_METRIC_TYPES),
    path('regions/', RegionsView.as_view(), name=URL_REGIONS),
]
