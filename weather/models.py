from django.db import models



class MetricType(models.Model):
    name = models.CharField(max_length=63, unique=True, primary_key=True)
    unit = models.CharField(max_length=15)

    def __str__(self):
        return f"{self.name} ({self.unit})"


class Region(models.Model):
    name = models.CharField(max_length=63, unique=True, primary_key=True)

    def __str__(self):
        return self.name


class Metric(models.Model):
    value = models.FloatField()
    date = models.DateField()
    metric_type = models.ForeignKey(MetricType, related_name="metrics", on_delete=models.CASCADE)
    region = models.ForeignKey(Region, related_name="metrics", on_delete=models.CASCADE)

    @property
    def year(self):
        return self.date.year

    @property
    def month(self):
        return self.date.month

    def __str__(self):
        return f"{self.metric_type.name}: {self.value} {self.metric_type.unit} ({self.year}-{self.month})"
