from rest_framework import generics
from .serializers import MetricSerializer, MetricTypeSerializer, RegionSerializer
from .constants import *
from .models import Metric, MetricType, Region
from rest_framework.exceptions import ValidationError
from datetime import date
from django.utils import dateparse


class MetricTypesView(generics.ListCreateAPIView):
    serializer_class = MetricTypeSerializer
    queryset = MetricType.objects.all()


class RegionsView(generics.ListCreateAPIView):
    serializer_class = RegionSerializer
    queryset = Region.objects.all()


class MetricsView(generics.ListCreateAPIView):
    serializer_class = MetricSerializer

    def get_queryset(self):
        """
        fetch query_params from the request and apply filters to the query_set accordingly
        :return: the filtered query_set
        """
        start_date = dateparse.parse_date(
            self.request.query_params.get(KEY_START_DATE)) if KEY_START_DATE in self.request.query_params else None
        end_date = dateparse.parse_date(
            self.request.query_params.get(KEY_END_DATE)) if KEY_END_DATE in self.request.query_params else None
        region = self.request.query_params.get(KEY_REGION, None)
        metric_type = self.request.query_params.get(KEY_TYPE, None)

        query_set = Metric.objects.all()

        if start_date and not end_date:
            end_date = date(year=MAX_YEAR, month=MAX_MONTH, day=DEFAULT_DAY)

        if end_date and not start_date:
            start_date = date(year=MIN_YEAR, month=MIN_MONTH, day=DEFAULT_DAY)

        if end_date and start_date and start_date > end_date:
            raise ValidationError(detail="Start date cannot be after end date")

        if start_date and end_date:
            query_set = query_set.filter(date__gte=start_date, date__lte=end_date)

        if region:
            query_set = query_set.filter(region__name=region)

        if metric_type:
            query_set = query_set.filter(metric_type__name=metric_type)

        query_set = query_set.order_by("-date")

        return query_set
