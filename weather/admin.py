from django.contrib import admin
from .models import Metric, MetricType, Region

admin.site.register(Metric)
admin.site.register(MetricType)
admin.site.register(Region)
