from rest_framework import serializers
from .constants import *
from .models import Metric, MetricType, Region
from datetime import date
from django.db.models import ObjectDoesNotExist


class MetricTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = MetricType
        fields = '__all__'


class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = '__all__'


class MetricSerializer(serializers.Serializer):
    value = serializers.FloatField(required=True)
    month = serializers.IntegerField(required=True, max_value=MAX_MONTH, min_value=MIN_MONTH)
    year = serializers.IntegerField(required=True, max_value=MAX_YEAR, min_value=MIN_YEAR)
    metric_type = serializers.CharField(required=True, max_length=23)
    region = serializers.CharField(required=True, max_length=23)

    def validate_metric_type(self, metric_type_name):
        """
        Validate that metric_type already exists and return the actual object
        :param metric_type_name: the raw metric_type
        :return: MetricType object
        """
        try:
            return MetricType.objects.get(name=metric_type_name)
        except ObjectDoesNotExist:
            raise serializers.ValidationError(detail=f"Metric type: {metric_type_name} doesn't exist")

    def validate_region(self, region_name):
        """
        Validate that region already exists and return the actual object
        :param region_name: the raw region_name
        :return: Region object
        """
        try:
            return Region.objects.get(name=region_name)
        except ObjectDoesNotExist:
            raise serializers.ValidationError(detail=f"Region: {region_name} doesn't exist")

    def create(self, validated_data):
        """
        Creates a Metric object using the validated data and convert month and year to date
        :param validated_data: validated data
        :return: the saved Metric object
        """
        metric = Metric(
            value=validated_data.get(KEY_VALUE),
            date=date(year=validated_data.get(KEY_YEAR), month=validated_data.get(KEY_MONTH), day=DEFAULT_DAY),
            metric_type=validated_data.get(KEY_TYPE),
            region=validated_data.get(KEY_REGION),
        )
        metric.save()
        return metric

    def update(self, instance, validated_data):
        """
        updates and existing Metric object using the validated data and convert month and year to date
        :param validated_data: validated data
        :return: the saved Metric object
        """
        instance.value = validated_data.get(KEY_VALUE, instance.value)
        year = validated_data.get(KEY_YEAR, instance.date.year)
        month = validated_data.get(KEY_MONTH, instance.date.month)
        instance.date = date(year=year, month=month, day=DEFAULT_DAY)
        instance.metric_type = validated_data.get(KEY_TYPE, instance.type_name)
        instance.region = validated_data.get(KEY_REGION, instance.region_name)
        instance.save()
        return instance
